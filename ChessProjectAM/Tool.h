#pragma once
#include <iostream>
class Tool
{
private:
	char _type;
public:
	Tool(char type);
	Tool(Tool& s);
	~Tool();
	virtual void setType(const char type);
	virtual char getType();
	virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index,char dest);
};