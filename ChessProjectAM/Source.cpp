/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Game.h"
#include "Tool.h"
#include "Bishop.h"
#include "Queen.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "King.h"


using std::cout;
using std::endl;
using std::string;


void main()
{

	srand(time_t(NULL));
	Bishop white_bishop('B');
	King white_King('K');
	Knight white_Knight('N');
	Pawn white_Pawn('P');
	Queen white_Queen('Q');
	Rook white_Rook('R');


	Bishop black_bishop('b');
	King black_King('k');
	Knight black_Knight('n');
	Pawn black_Pawn('p');
	Queen black_Queen('q');
	Rook black_Rook('r');


	Game game(&black_Rook, &black_King, &black_Knight, &black_Pawn, &black_Queen, &black_bishop ,&white_Rook, &white_King, &white_Knight, &white_Pawn, &white_Queen, &white_bishop);

	
	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		strcpy_s(msgToGraphics, game.canMove(msgFromGraphics)); // msgToGraphics should contain the result of the operation

		/*//JUST FOR EREZ DEBUGGING
		int r = rand() % 10; // just for debugging......
		msgToGraphics[0] = (char)(1 + '0');
		msgToGraphics[1] = 0;
		// JUST FOR EREZ DEBUGGING */


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}