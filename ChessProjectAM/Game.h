#pragma once
#include "Tool.h"
#include <iostream>

class Game
{
public:
	int* get_king_coords(int src_x_index, int src_y_index, bool counter_chess);
	bool chess_state(int,int, int, int);
	bool outOfChess(int src_x_index, int src_y_index, int dst_x_index, int dst_y_index);
	const char* canMove(std::string request);
	void move(int src_x_index, int src_y_index, int dst_x_index, int dst_y_index);
	Game(Tool* Rook, Tool* King, Tool* Knight, Tool* Pawn, Tool* Queen, Tool* Bishop, Tool* R_Rook, Tool* K_King, Tool* K_Knight, Tool* P_Pawn, Tool* Q_Queen, Tool* B_Bishop);
	~Game();
	Tool* get_tool(char);
	void setTurn(int turn);
	static int convertLetter(char);
	char get_dst_value(int,int);
private:
	int _turn;
	char _board[8][8];
	Tool* _tools[12];
};