#include "Tool.h"
#include <iostream>
class Rook : public Tool 
{
	public:
		Rook(char type);
		~Rook();
		virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest);
	private:
};