#include "king.h"

King::King(char type) : Tool(type)
{
}
King::~King()
{

}
bool King::canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest)
{
	int i = 0, j = 0;
	for (i = src_y_index+1; i > src_y_index -2; i--)
	{
		for (j = src_x_index - 1; j <src_x_index + 2; j++)
		{
			if (dst_x_index == j && i == dst_y_index)
			{
				return true;
			}
		}
	}
	return false;
}
