#include "Tool.h"
#include <iostream>
class Queen : public Tool
{
public:
	Queen(char type);
	~Queen();
	virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest);
private:
};