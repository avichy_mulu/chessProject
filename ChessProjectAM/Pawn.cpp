#include "Pawn.h"
Pawn::Pawn(char type) : Tool(type)
{
}
Pawn::~Pawn()
{
}
bool Pawn::canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index,char dest)
{
	if ((this->getType() == 'P' && src_y_index == 1))// checking if this is the first move of the pawn
	{
		if ((dst_y_index == (src_y_index + 2) || dst_y_index == (src_y_index + 1)) && src_x_index == dst_x_index)//if its the first move of the pawn it can move twice forward
		{
			return true;
		}
	}
	else if ((this->getType() == 'p' && src_y_index == 6))//black tools move "backwards"
	{
		if ((dst_y_index == (src_y_index -2) || dst_y_index == (src_y_index -1)) && src_x_index == dst_x_index)//if its the first move of the pawn it can move twice forward
		{
			return true;
		}
	}
	if(this->getType() == 'p')//black tools move "backwards"
	{
		if (dst_y_index == (src_y_index -1) && src_x_index == dst_x_index)
		{
			return true;
		}
		if (dest!= '#')
		{
			if (dst_x_index == src_x_index - 1 && dst_y_index == src_y_index - 1)
			{
				return true;
			}
			if (dst_x_index == src_x_index + 1 && dst_y_index == src_y_index - 1)
			{
				return true;
			}
		}
	}
	else if (this->getType() == 'P')//white tools move "forwards"
	{
		if (dst_y_index == (src_y_index +1) && src_x_index == dst_x_index)
		{
			return true;
		}
		if (dest!='#')
		{
			if (dst_x_index == src_x_index + 1 && dst_y_index == src_y_index + 1)
			{
				return true;
			}
			if (dst_x_index == src_x_index - 1 && dst_y_index == src_y_index + 1)
			{
				return true;
			}
		}
	}
	return false;
}