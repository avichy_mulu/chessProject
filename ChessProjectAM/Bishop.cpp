#include "Bishop.h"

Bishop::Bishop(char type) : Tool(type)
{
}


Bishop::~Bishop() {}



bool Bishop::canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest)
{
	if (dst_x_index != src_x_index && dst_y_index != src_y_index && (src_x_index - dst_x_index) == (src_y_index-dst_y_index))
		return true;
	return false;
}