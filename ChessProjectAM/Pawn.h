#include "Tool.h"
#include <iostream>
class Pawn : public Tool
{
public:
	Pawn(char type);
	~Pawn();
	virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest);
private:
};