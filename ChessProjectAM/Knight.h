#include "Tool.h"
#include <iostream>
class Knight : public Tool 
{
public:
	Knight(char type);
	~Knight();
	virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest);
private:
};