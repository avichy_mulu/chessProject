#include "Game.h"
#include <iostream>
#include <string>


int Game::convertLetter(char letter)//the funcs gets a letter and returns the in equivelent index  in the board
{
	switch (letter)
	{
	case 'a':
		return 0;
	case 'b':
		return 1;
	case 'c':
		return 2;
	case 'd':
		return 3;
	case 'e':
		return 4;
	case 'f':
		return 5;
	case 'g':
		return 6;
	case 'h':
		return 7;
	case '1':
		return 0;
	case '2':
		return 1;
	case '3':
		return 2;
	case '4':
		return 3;
	case '5':
		return 4;
	case '6':
		return 5;
	case '7':
		return 6;
	case '8':
		return 7;
	};
}


const char* Game::canMove(std::string request)
{
	int i = 0, j = 0, src_x_index = convertLetter(request[0]), src_y_index = convertLetter(request[1]), dst_x_index = convertLetter(request[2]), dst_y_index = convertLetter(request[3]);
	if ((_board[src_x_index][src_y_index] == '#') || (this->_turn==1 && isupper(_board[src_x_index][src_y_index])) || (this->_turn ==0 && islower(_board[src_x_index][src_y_index])))// if there were no tool choosed or the turn is not his tool turn
	{
		return "2";
	}
	else if (dst_x_index < 0 || dst_y_index < 0 || dst_x_index>7 || dst_y_index >7)//if the indexes are not compatible
	{
		return "5";
	}
	if ((islower(_board[src_x_index][src_y_index]) && islower(_board[dst_x_index][dst_y_index])) || (isupper(_board[src_x_index][src_y_index]) && isupper(_board[dst_x_index][dst_y_index])))// if a tool from the same color is in the destination index
	{
		return "3";
	}
	else if (src_x_index == dst_x_index && src_y_index == dst_y_index)//same tool choosed as source and dest
	{
		return "7";
	}
	else if (!outOfChess(src_x_index, src_y_index,dst_x_index,dst_y_index))// check if the movment will make the game be in chess state
	{
		return "4";
	}
	else if (chess_state(src_x_index, src_y_index, dst_x_index, dst_y_index) && (get_tool(_board[src_x_index][src_y_index])->canReach(dst_x_index, dst_y_index, src_x_index, src_y_index,get_dst_value(dst_x_index,dst_y_index))))//if the tool movment wiil make the game be in chess state
	{
		if (this->_turn == 1)
			this->setTurn(0);
		else
			this->setTurn(1);
		return "1";
	}
	else if (get_tool(_board[src_x_index][src_y_index])->canReach(dst_x_index, dst_y_index, src_x_index, src_y_index,get_dst_value(dst_x_index,dst_y_index)))
	{
		move(src_x_index,src_y_index,dst_x_index,dst_y_index);
		if (this->_turn == 1)
			this->setTurn(0);
		else
			this->setTurn(1);
		return "0";
	}
	std::cout << _board[src_x_index][src_y_index] << std::endl;
	return "6";
}


void  Game::move(int src_x_index, int src_y_index, int dst_x_index, int dst_y_index)
{
	_board[dst_x_index][dst_y_index] = '#';
	std::swap(_board[dst_x_index][dst_y_index],_board[src_x_index][src_y_index]);
}


Game::Game(Tool* Rook, Tool* King, Tool* Knight, Tool* Pawn, Tool* Queen, Tool* Bishop, Tool* R_Rook, Tool* K_King, Tool* K_Knight, Tool* P_Pawn, Tool* Q_Queen, Tool* B_Bishop)//the objects with the big letters before their name are the white tools
{
	int i = 0,j=0;
	this->_turn = 1;
	_tools[0] = Rook, _tools[1] = King, _tools[2] = Knight, _tools[3] = Pawn, _tools[4] = Queen, _tools[5] = Bishop, _tools[6] = R_Rook, _tools[7] = K_King, _tools[8] = K_Knight, _tools[9] = P_Pawn, _tools[10] = Q_Queen, _tools[11] = B_Bishop;
	_board[0][0] =  'R', _board[1][0] = 'N', _board[2][0] = 'B', _board[3][0] = 'K', _board[4][0] = 'Q', _board[5][0] = 'B', _board[6][0] = 'N', _board[7][0] = 'R';
	for (i = 0; i < 8; i++)
	{
		_board[i][1] = 'P';
	}
	for (i =2; i < 6; i++)
	{
		for(j=0;j<8;j++)
			_board[j][i] = '#';
	}
	for (i = 0; i < 8; i++)
	{
		_board[i][6] = 'p';
	}
	_board[0][7] = 'r', _board[1][7]  = 'n', _board[2][7]  = 'b', _board[3][7]  = 'k', _board[4][7]  = 'q', _board[5][7]  = 'b', _board[6][7]  = 'n', _board[7][7] = 'r';
}


Game::~Game()
{

}


void Game::setTurn(int turn)
{
	this->_turn = turn;
}


bool Game::chess_state(int src_x_index, int src_y_index, int dst_x_index, int dst_y_index)// the func checks if the game is in chess state if the curr move will be applied
{
	int i = 0, j = 0, king_x_index = 0, king_y_index = 0;
	king_x_index = get_king_coords(src_x_index,src_y_index,false)[0];
	king_y_index = get_king_coords(src_x_index, src_y_index,false)[1];
	if (get_tool(_board[src_x_index][src_y_index])->canReach(king_x_index, king_y_index, dst_x_index, dst_y_index, get_dst_value(king_x_index, king_y_index)))
	{
		return true;
	}
	return false;
}

int* Game::get_king_coords(int src_x_index, int src_y_index,bool counter_chess)
{
	char king_type = ' ';
	int king_coords[2] = {0,0},i=0,j=0;
	if (isupper(_board[src_x_index][src_y_index]))// meaning the tool is white
	{
		if(counter_chess)
			king_type = 'K';
		king_type = 'k';
	}

	else if (islower(_board[src_x_index][src_y_index]))// meaning the tool is black
	{
		if (counter_chess)
			king_type = 'k';
		king_type = 'K';
	}


	for (i = 0; i < 8; i++)//a loop checking what are the opposite king Coordinates
	{
		for (j = 0; j < 8; j++)
		{
			if ((_board[j][i]) == king_type)
			{
				king_coords[0] = j;
				king_coords[1] = i;
				return king_coords;
			}
		}
	}

}

bool Game::outOfChess(int src_x_index, int src_y_index, int dst_x_index, int dst_y_index)
{
	int i = 0, j = 0, king_x_index = 0, king_y_index = 0;
	char temp = 0,temp2=0;
	bool still_in_chess = false;
	king_x_index = get_king_coords(src_x_index, src_y_index,true)[0];
	king_y_index = get_king_coords(src_x_index, src_y_index,true)[1];
	for (i=0;i<8;i++)
	{
		for (j = 0; j < 8; j++)
		{
			if ((islower(_board[king_x_index][king_y_index]) && isupper(_board[j][i])) || (islower(_board[j][i]) && isupper(_board[king_x_index][king_y_index])))// checking the opposites of the king
			{
				if (get_tool(_board[j][i])->canReach(king_x_index, king_y_index, j, i, get_dst_value(king_x_index, king_y_index)))// if the game is in chess state
				{
					temp = _board[dst_x_index][dst_y_index];
					temp2 = _board[src_x_index][src_y_index];
					_board[dst_x_index][dst_y_index] = '#';
					std::swap(_board[dst_x_index][dst_y_index], _board[src_x_index][src_y_index]);
					if (get_tool(_board[j][i])->canReach(king_x_index, king_y_index,j, i, get_dst_value(king_x_index, king_y_index)))
					{
						still_in_chess = true;
					}
					_board[src_x_index][src_y_index] = temp;
					std::swap(_board[dst_x_index][dst_y_index], _board[src_x_index][src_y_index]);
				}
			}
		}
	}
	if (still_in_chess)
	{
		return false;
	}
	std::cout<< "b4: " << temp2<< std::endl;
	std::cout << "after: "<< _board[src_x_index][src_y_index] <<std::endl;
	return true;
}

Tool* Game::get_tool(char letter)
{
	switch (letter)
	{
	case 'r':
		return _tools[0];
	case 'k':
		return _tools[1];
	case 'n':
		return _tools[2];
	case 'p':
		return _tools[3];
	case 'q':
		return _tools[4];
	case 'b':
		return _tools[5];



	case 'R':
		return _tools[6];
	case 'K':
		return _tools[7];
	case 'N':
		return _tools[8];
	case 'P':
		return _tools[9];
	case 'Q':
		return _tools[10];
	case 'B':
		return _tools[11];
	};
}




char Game::get_dst_value(int x,int y)
{
	return _board[x][y];
}