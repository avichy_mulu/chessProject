#include "Tool.h"

Tool::Tool(char type)
{
	_type = type;
};

Tool::~Tool()
{
};

char Tool::getType()
{
	return _type;
}


bool Tool::canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest)
{
	return false;
}

void Tool::setType(const char type)
{
	this->_type = type;
}


Tool::Tool(Tool& s)
{
	_type = s._type;
}