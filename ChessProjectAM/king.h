#include "Tool.h"
#include <iostream>
class King: public Tool
{
public:
	King(char type);
	~King();
	virtual bool canReach(int dst_x_index, int dst_y_index, int src_x_index, int src_y_index, char dest);
private:
};